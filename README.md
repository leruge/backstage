# 后台扩展
该扩展会自动生成后台需要的接口以及前端常用的接口，适用于thinkphp8

## 安装
- `composer require leruge/admin`

## 安装后说明
1. 移除 `topthink/think-trace`扩展
2. 移除gitignore中的env
3. 使用env，配置APP_URL地址，并配置数据库，编码utf8mb4，配置数据库前缀
4. 强制路由以及完全匹配
5. 删除public/static下的.gitignore
6. 删除app下的controller目录
7. .htaccess文件的伪静态修改
8. 搭建后台 `php think admin:create`
9. 接管异常配置 `CustomerException`
10. 允许跨域
11. 配置上传目录，filesystem.php
```php
'customer' => [
    'type' => 'local',
    'root' => app()->getRootPath() . 'public' . DIRECTORY_SEPARATOR . 'static' . DIRECTORY_SEPARATOR . 'uploads',
]
```
12. 配置独立错误日志

## 使用说明

## 待开发
1. 官网首页
2. 下载页面
3. 用户协议页面

## 联系方式
- email：leruge@163.com
- qq：305530751
- wx：lerage