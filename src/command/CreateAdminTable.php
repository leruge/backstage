<?php
declare(strict_types = 1);

namespace leruge\command;

use think\console\Command;

class CreateAdminTable extends Command
{
    public function configure()
    {
        $this->setName('admin:table')
            ->setDescription('创建后台基础表');
    }

    public function handle()
    {
        if (!$this->app->has('migration.creator')) {
            $this->output->error('请安装`think-migration`扩展');
            return;
        }

        $className = 'CreateAdminTables';
        $creator = $this->app->get('migration.creator');
        $path = $creator->create($className);
        $contents = file_get_contents(__DIR__ . '/stubs/admin.stub');
        file_put_contents($path, $contents);
        $this->output->info('后台迁移文件生成成功');
    }
}