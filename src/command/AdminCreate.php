<?php
declare(strict_types = 1);

namespace leruge\command;

use think\console\Command;
use think\facade\Console;

class AdminCreate extends Command
{
    public function configure()
    {
        $this->setName('admin:create')
            ->setDescription('搭建后台');
    }

    public function handle()
    {
        // 生成后台迁移
        Console::call('admin:table');
        // 生成权限迁移
        sleep(1);
        Console::call('auth:table');
        // 生成权限模型
        Console::call('auth:model');

        // 复制文件
        Console::call('admin:generate');

        // 执行迁移
        Console::call('migrate:run');

        // 种子填充
        Console::call('seed:run');

        $this->output->info('后台搭建成功');
    }
}