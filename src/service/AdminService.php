<?php
declare(strict_types = 1);

namespace leruge\service;

use leruge\command\AdminCreate;
use leruge\command\CreateAdminTable;
use leruge\command\GenerateFile;
use think\Service;

class AdminService extends Service
{
    public function boot(): void
    {
        $this->commands([
            CreateAdminTable::class,
            AdminCreate::class,
            GenerateFile::class,
        ]);
    }
}