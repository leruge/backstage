<?php
/**
 * @title 其它配置
 * @author Leruge
 * @email leruge@163.com
 */

return [
    'app_url' => env('app_url', 'http://admin.io/')
];
